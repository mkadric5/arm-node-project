var http = require('http');

var onRequest = (req, resp) => {
    resp.writeHead(200, {'Content-Type': 'text/html'});
    let date = new Date(Date.now()).toLocaleString();
    let student = 'Mirza Kadric';
    let template = '<h1>' + student + '</h1>' + '<h3>' + date + '</h3>'
    resp.write(template);
    resp.end();
}

http.createServer(onRequest).listen(8000);
